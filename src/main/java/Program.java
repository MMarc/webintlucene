import cluster.ClusterAlgorithmExecution;
import common.*;
import index.FileIndexer;
import index.ResourceDocumentProvider;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by marc on 08.06.17.
 * Main Program
 */
public class Program {
    public static void main(String[] argv) throws URISyntaxException, IOException {
        Configuration configuration = new Configuration(
                Localization.EN,
                IndexPersistence.TEMPORARY,
                "",
                "complexFilesEnglish"
        );
        ResourceDocumentProvider testfiles = new ResourceDocumentProvider(configuration);
        AnalyzerFactoryImpl analyzerFactory = new AnalyzerFactoryImpl(configuration);
        IndexDirectoryBuilder indexDirectoryBuilder = new IndexDirectoryBuilder(configuration);
        StemmerFactory stemmerFactory = new StemmerFactory(configuration);

        FileIndexer fileIndexer =
                new FileIndexer(indexDirectoryBuilder, analyzerFactory)
                        .createIndex(testfiles);
        System.out.println("Index creation succeed");

        new ClusterAlgorithmExecution(indexDirectoryBuilder, analyzerFactory, stemmerFactory)
                .initialize()
                .start();

        indexDirectoryBuilder.closeDirectory();
    }
}
