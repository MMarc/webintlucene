package common;

import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

/**
 * Created by marc on 12.06.17.
 */
public class AnalyzerFactory {
    private final Configuration configuration;

    public AnalyzerFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    public org.apache.lucene.analysis.Analyzer createAnalyzer() {
        switch (this.configuration.getLocalization()) {
            case DE:
                return new GermanAnalyzer();
            case EN:
                return new EnglishAnalyzer();
            default:
                return new StandardAnalyzer();
        }
    }


}
