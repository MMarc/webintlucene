package common;

/**
 * Created by marc on 08.06.17.
 * Required constants for lucene
 */
public class LuceneConstants {
    public static final String CONTENTS = "contents";
    public static final String FILE_NAME = "filename";
    public static final String FILE_PATH = "filepath";
    public static final int MAX_SEARCH = 10;
    public static final String PRIMARY_KEY = "pk";
}
