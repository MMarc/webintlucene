package common;

import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

/**
 * Created by marc on 08.06.17.
 * Class which contains some field types for lucene documents
 */
public class FieldTypeContainer {

    private static final FieldTypeContainer fieldTypeContainer = new FieldTypeContainer();


    private FieldTypeContainer() {

    }

    public static FieldTypeContainer instance(){
        return fieldTypeContainer;
    }

    public FieldType getContentFieldType() {
        FieldType fieldType = new FieldType();
        fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        fieldType.setStored(true);
        fieldType.setTokenized(true);
        fieldType.setStoreTermVectors(true);
        fieldType.setStoreTermVectorPositions(true);
        fieldType.setStoreTermVectorOffsets(true);
        fieldType.setStoreTermVectorPayloads(true);
        return fieldType;
    }


    public FieldType getTitleFieldType() {
        FieldType fieldType = new FieldType();
        fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        fieldType.setStored(true);
        fieldType.setTokenized(true);
        return fieldType;
    }
}
