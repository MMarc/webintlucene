package common;

/**
 * Created by marc on 13.06.17.
 * Represents the current configuration
 */
public class Configuration {
    private final Localization localization;
    private final IndexPersistence indexPersistence;
    private final String persistentDirectoryPath;
    private final String indexFilesDirectory;

    public Configuration(
            Localization localization,
            IndexPersistence indexPersistence,
            String persistentDirectoryPath,
            String indexFilesDirectory) {
        this.localization = localization;
        this.indexPersistence = indexPersistence;
        this.persistentDirectoryPath = persistentDirectoryPath;
        this.indexFilesDirectory = indexFilesDirectory;
    }

    public Localization getLocalization() {
        return this.localization;
    }

    public IndexPersistence getIndexPersistence() {
        return this.indexPersistence;
    }

    public String getPersistentDirectoryPath() {
        return this.persistentDirectoryPath;
    }

    public String getIndexFilesDirectory() {
        return this.indexFilesDirectory;
    }
}
