package common;

import cluster.Stemmer;

/**
 * Created by marc on 13.06.17.
 */
public class StemmerFactory {
    private final Configuration configuration;

    public StemmerFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    public Stemmer createStemmer() {
        return new Stemmer(configuration);
    }
}
