package common;

/**
 * Created by marc on 13.06.17.
 * Represents the current locale
 */
public enum Localization {
    DE,
    EN,
    STANDARD
}
