package common;

import java.util.*;

/**
 * Created by marc on 12.06.17.
 */
public class TopWordBuilder {
    private final HashMap<String, Integer> importantTermOccurences = new HashMap<>();
    private TreeMap<String, Integer> importantTermOccurencesSorted;

    public TopWordBuilder() {
        ValueComparator valueComparator = new ValueComparator(this.importantTermOccurences);
        this.importantTermOccurencesSorted = new TreeMap<>(valueComparator);
    }


    public void addWords(List<String> words) {
        for(String word : words) {
            if (this.importantTermOccurences.containsKey(word)) {
                this.importantTermOccurences.put(word, this.importantTermOccurences.get(word) + 1);
            } else {
                this.importantTermOccurences.put(word, 1);
            }
        }
    }

    public List<String> getTop10Words(){
        this.importantTermOccurencesSorted.putAll(this.importantTermOccurences);
        List<String> top10ImportantTerms = new LinkedList<>();
        List<String> importantTerms = new ArrayList<>();
        importantTerms.addAll(this.importantTermOccurencesSorted.keySet());
        Collections.reverse(importantTerms);
        for(int i = 0; i <= 10 && i<importantTerms.size(); i++) {
            top10ImportantTerms.add(importantTerms.get(i));
        }
        return top10ImportantTerms;
    }

    public void cleanUp() {
        this.importantTermOccurences.clear();
        this.importantTermOccurencesSorted.clear();
    }

    //copied from https://stackoverflow.com/questions/15436516/get-top-10-values-in-hash-map
    class ValueComparator implements Comparator<String> {

        Map<String, Integer> base;
        public ValueComparator(HashMap<String, Integer> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            }
            // returning 0 would merge keys
        }
    }
}
