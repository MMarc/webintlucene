package common;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.RAMDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by marc on 13.06.17.
 */
public class IndexDirectoryBuilder {
    private final Configuration configuration;
    private Directory directory;

    public IndexDirectoryBuilder(Configuration configuration) {

        this.configuration = configuration;
    }

    public Directory getDirectory() {
        if (this.directory == null) {
            this.directory = this.createDirectory();
        }
        return this.directory;
    }

    private Directory createDirectory() {
        switch (this.configuration.getIndexPersistence()) {
            case TEMPORARY:
                return new RAMDirectory();
            default:
                try {
                    return FSDirectory.open(Paths.get(this.configuration.getPersistentDirectoryPath()));
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Switching to RAMDirectory");
                    return new RAMDirectory();
                }
        }
    }

    public void closeDirectory() {
        try {
            this.directory.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
