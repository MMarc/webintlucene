package index;

import common.AnalyzerFactory;
import common.IndexDirectoryBuilder;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * Created by marc on 08.06.17.
 * Wrapper class to create a file index
 */
public class FileIndexer {

    private final IndexWriter indexWriter;

    public FileIndexer(IndexDirectoryBuilder indexDirectoryBuilder, AnalyzerFactory analyzerFactory) throws IOException {
        Directory directory = indexDirectoryBuilder.getDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzerFactory.createAnalyzer());
        indexWriter = new IndexWriter(directory, config);
    }

    public FileIndexer createIndex(ResourceDocumentProvider resourceDocumentProvider) {
        List<Document> documents = IndexCreationHelper.convertToDocumentFrom(resourceDocumentProvider.getFiles());
        System.out.println("Creating file index with " + documents.size() + " files");
        this.addDocuments(documents);
        return this;
    }

    private void addDocuments(List<Document> documents) {
        try {
            this.indexWriter.addDocuments(documents);
            this.indexWriter.commit();
            this.indexWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
