package index;

import common.Configuration;

import java.io.File;
import java.net.URL;

/**
 * Created by marc on 08.06.17.
 * This class provides files which are used to create a file index
 */
public class ResourceDocumentProvider {
    private final Configuration configuration;

    public ResourceDocumentProvider(Configuration configuration) {
        this.configuration = configuration;
    }

    File[] getFiles() {
        URL resource = ResourceDocumentProvider.class.getResource(this.configuration.getIndexFilesDirectory());
        File fileDirectory = new File(resource.getPath());
        return fileDirectory.listFiles();
    }

}
