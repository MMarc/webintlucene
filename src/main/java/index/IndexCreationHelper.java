package index;

import common.FieldTypeContainer;
import common.LuceneConstants;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marc on 08.06.17.
 * Helper methods to read files and create documents
 */
public class IndexCreationHelper {
    private static Document createDocument(File file) {
        Document document = new Document();

        Field contentField =
                new Field(LuceneConstants.CONTENTS, readFile(file, Charset.defaultCharset()), FieldTypeContainer.instance().getContentFieldType());

        Field fileNameField =
                new Field(LuceneConstants.FILE_NAME, file.getName(), FieldTypeContainer.instance().getTitleFieldType());

        document.add(fileNameField);
        document.add(contentField);

        return document;
    }

    public static List<Document> convertToDocumentFrom(File[] files) {
        List<Document> documents = new ArrayList<>();
        for (File file : files) {
            Document doc = createDocument(file);
            documents.add(doc);
        }
        return documents;
    }

    private static String readFile(File file, Charset encoding)
    {
        try {
            byte[] encoded = Files.readAllBytes(file.toPath());
            return new String(encoded, encoding);
        } catch (IOException ex) {
            System.out.println("Could not read file: " + file.getAbsolutePath());
            return "";
        }
    }

}
