package cluster;

import common.LuceneConstants;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.search.Query;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

/**
 * Created by marc on 12.06.17.
 * Used to create sub cluster.
 */
public class MoreLikeThisSubClusterBuilder implements SubClusterBuilder {

    private final String parentClusterQuery;
    private final IndexReader indexReader;
    private final Analyzer analyzer;
    private final HashMap<String, Integer> importantTermOccurences = new HashMap<>();
    private TreeMap<String, Integer> importantTermOccurencesSorted;

    public MoreLikeThisSubClusterBuilder(String parentClusterQuery, IndexReader indexReader, Analyzer analyzer) {
        this.parentClusterQuery = parentClusterQuery;
        this.indexReader = indexReader;
        this.analyzer = analyzer;
        ValueComparator valueComparator = new ValueComparator(this.importantTermOccurences);
        this.importantTermOccurencesSorted = new TreeMap<>(valueComparator);
    }

    @Override
    public void analyzeDocument(Document document, int documentId) throws IOException {
        String s = document.getField(LuceneConstants.CONTENTS).stringValue();
        StringReader stringReader = new StringReader(s);
        MoreLikeThis moreLikeThis = new MoreLikeThis(this.indexReader);
        moreLikeThis.setAnalyzer(this.analyzer);
        moreLikeThis.setMinTermFreq(1);
        moreLikeThis.setMinDocFreq(1);
        //Query like1 = moreLikeThis.like(document.getField(LuceneConstants.PRIMARY_KEY).numericValue().intValue());
        Query query =
                moreLikeThis.like(LuceneConstants.CONTENTS, stringReader);
        String[] importantTermsOfDocument = query.toString(LuceneConstants.CONTENTS).split(" ");
        this.addTermsToRankTable(importantTermsOfDocument);
        //System.out.println(like1.toString(LuceneConstants.CONTENTS));
    }

    private void addTermsToRankTable(String[] terms) {
        for(String term : terms) {
            this.addTermToRankTable(term);
        }
    }

    private void addTermToRankTable(String term) {
        if(this.importantTermOccurences.containsKey(term)) {
            this.importantTermOccurences.put(term, this.importantTermOccurences.get(term) + 1);
        } else {
            this.importantTermOccurences.put(term, 1);
        }
    }

    /**
     * Creates the top 10 of the analyzed documents.
     * @return Query of the top 10 terms
     */
    @Override
    public List<String> getTop10ImportantTerms() {
        this.importantTermOccurencesSorted.putAll(this.importantTermOccurences);
        List<String> top10ImportantTerms = new LinkedList<>();
        List<String> importantTerms = new ArrayList<>();
        importantTerms.addAll(this.importantTermOccurencesSorted.keySet());
        importantTerms.remove(this.parentClusterQuery);
        Collections.reverse(importantTerms);
        for(int i = 0; i <= 10 && i<importantTerms.size(); i++) {
            top10ImportantTerms.add(this.parentClusterQuery + " " + importantTerms.get(i));
        }
        return top10ImportantTerms;
    }

    @Override
    public void cleanUp() {
        this.importantTermOccurences.clear();
    }

    //copied from https://stackoverflow.com/questions/15436516/get-top-10-values-in-hash-map
    class ValueComparator implements Comparator<String> {

        Map<String, Integer> base;
        public ValueComparator(HashMap<String, Integer> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            }
            // returning 0 would merge keys
        }
    }
}


