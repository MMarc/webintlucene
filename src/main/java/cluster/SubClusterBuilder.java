package cluster;

import org.apache.lucene.document.Document;

import java.io.IOException;
import java.util.List;

/**
 * Created by marc on 12.06.17.
 */
public interface SubClusterBuilder {
    void analyzeDocument(Document document, int documentId) throws IOException;

    List<String> getTop10ImportantTerms();

    void cleanUp();
}
