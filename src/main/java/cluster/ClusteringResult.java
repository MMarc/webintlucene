package cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marc on 09.06.17.
 * Contains the results of a cluster cycle.
 */
public class ClusteringResult {
    private List<String> matchedFiles;
    private List<String> suggestedCluster;
    private final String clusterQuery;

    public ClusteringResult(List<String> matchedFiles, List<String> suggestedCluster, String clusterQuery) {

        this.matchedFiles = matchedFiles;
        this.suggestedCluster = suggestedCluster;
        this.clusterQuery = clusterQuery;
    }

    public ClusteringResult(String clusterQuery){
        this.clusterQuery = clusterQuery;
        this.matchedFiles = new ArrayList<>();
        this.suggestedCluster = new ArrayList<>();
    }

    public void addSuggestedClusters(List<String> clusterNames) {
        this.suggestedCluster.addAll(clusterNames);
    }

    public void addMatchedFile(String fileName) {
        this.matchedFiles.add(fileName);
    }

    public void addSuggestedCluster(String clusterName) {
        this.suggestedCluster.add(clusterName);
    }

    public List<String> getMatchedFiles() {
        return this.matchedFiles;
    }

    public List<String> getSuggestedCluster() {
        return this.suggestedCluster;
    }

    public String getClusterQuery() {
        return this.clusterQuery;
    }
}
