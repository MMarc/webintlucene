package cluster;

import common.LuceneConstants;
import common.StemmerFactory;
import common.TopWordBuilder;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.StopwordAnalyzerBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.tartarus.snowball.ext.German2Stemmer;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by marc on 12.06.17.
 * Implements the standard keyword generator
 * copied from https://github.com/shamikm/similarity/blob/master/src/main/java/org/maj/sm/KeywordGeneratorImpl.java
 */
public class StandardKeywordGenerator implements KeywordGenerator, SubClusterBuilder {
    private final Analyzer analyzer;
    private final Stemmer stemmer ; //GermanStemmer, GermanLightStemmer
    private final List<List<String>> keyWordLists = new ArrayList<>();
    private final String parentClusterName;
    private final TopWordBuilder topWordBuilder = new TopWordBuilder();

    public StandardKeywordGenerator(String parentClusterName,
                                    Analyzer analyzer,
                                    StemmerFactory stemmerFactory) {
        this(parentClusterName, null, analyzer, stemmerFactory);
    }

    private StandardKeywordGenerator(String parentClusterName, Set<?> stopWords,
                                     Analyzer analyzer,
                                     StemmerFactory stemmerFactory) {
        // stopWords == null ? new StandardAnalyzer(Version.LUCENE_36)  : new StandardAnalyzer(Version.LUCENE_36,stopWords);
        this.stemmer = stemmerFactory.createStemmer();
        this.parentClusterName = parentClusterName;
        this.analyzer = analyzer;
    }

    @Override
    public Set<String> generateKeyWords(String content) {
        Set<String> keywords = new HashSet<>();
        TokenStream stream = analyzer.tokenStream("contents", new StringReader(content));
        try {
            stream.reset();
            while(stream.incrementToken()) {
                String kw = stream.getAttribute(CharTermAttribute.class).toString();
                keywords.add(stemmer.stem(kw));
                keywords.add(kw);
            }
        }catch(Exception ex) {
            System.out.println("Error while generating key words: " + ex.getMessage());
        }finally {

            try {
                stream.end();
                stream.close();
            } catch (Exception e) {
                System.out.println("Error while generating key words (closing stream): " + e.getMessage());
            }
        }
        return keywords;
    }

    @Override
    public void analyzeDocument(Document document, int documentId) throws IOException {
        String documentContent = document.get(LuceneConstants.CONTENTS);
        Set<String> keyWords = this.generateKeyWords(documentContent);
        this.topWordBuilder.addWords(new ArrayList<>(keyWords));
    }

    @Override
    public List<String> getTop10ImportantTerms() {
        List<String> top10Words = this.topWordBuilder.getTop10Words();
        for(int i = 0; i < top10Words.size(); i++) {
            top10Words.set(i, this.parentClusterName + " " + top10Words.get(i));
        }
        return top10Words;
    }

    @Override
    public void cleanUp() {
        this.topWordBuilder.cleanUp();
    }
}
