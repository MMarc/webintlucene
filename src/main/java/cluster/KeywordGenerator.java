package cluster;

import java.util.Set;

/**
 * Created by marc on 12.06.17.
 * Copied from https://github.com/shamikm/similarity/blob/master/src/main/java/org/maj/sm/KeywordGenetorUsingCustomAnalyzer.java
 */
public interface KeywordGenerator {
    Set<String> generateKeyWords(String content);
}
