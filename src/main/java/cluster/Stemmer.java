package cluster;

import common.Configuration;
import org.tartarus.snowball.ext.German2Stemmer;
import org.tartarus.snowball.ext.PorterStemmer;

/**
 * Created by marc on 13.06.17.
 */
public class Stemmer {
    private final Configuration configuration;
    private final StemmerBase stemmer;

    public Stemmer(Configuration configuration) {
        this.configuration = configuration;
        this.stemmer = this.createStemmer();
    }

    public String stem(String term) {
        return this.stemmer.stem(term);
    }

    private StemmerBase createStemmer() {
        switch (this.configuration.getLocalization()) {
            case EN:
                return new EnglishStemmer();
            case DE:
                return new GermanStemmer();
        }
        return null;
    }

    private abstract class StemmerBase {
        public abstract String stem(String term);
    }

    private class EnglishStemmer extends StemmerBase {
        private final PorterStemmer porterStemmer = new PorterStemmer();

        @Override
        public String stem(String term) {
            this.porterStemmer.setCurrent(term);
            this.porterStemmer.stem();
            return this.porterStemmer.getCurrent();
        }
    }

    private class GermanStemmer extends StemmerBase {
        private final German2Stemmer german2Stemmer = new German2Stemmer();
        @Override
        public String stem(String term) {
            this.german2Stemmer.setCurrent(term);
            this.german2Stemmer.stem();
            return this.german2Stemmer.getCurrent();
        }
    }
}
