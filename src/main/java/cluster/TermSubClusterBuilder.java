package cluster;

import common.LuceneConstants;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.*;

/**
 * Created by marc on 12.06.17.
 */
public class TermSubClusterBuilder implements SubClusterBuilder {
    private final String parentClusterQuery;
    private final IndexReader indexReader;
    private List<List<String>> termOccurences = new ArrayList<>();

    public TermSubClusterBuilder(String parentClusterQuery, IndexReader indexReader) {
        this.parentClusterQuery = parentClusterQuery;
        this.indexReader = indexReader;
    }

    public void analyzeDocument(Document document, int documentId) throws IOException {
        List<String> result = new ArrayList<>();
        Terms terms = this.indexReader.getTermVector(documentId, LuceneConstants.CONTENTS);
        TermsEnum it = terms.iterator();
        for(BytesRef br = it.next(); br != null; br = it.next()) {
            result.add(br.utf8ToString());
        }
        this.termOccurences.add(result);
    }

    /**
     * Creates the top 10 of the analyzed documents.
     * @return Query of the top 10 terms
     */
    public List<String> getTop10ImportantTerms() {
        List<String> plainTermList = this.createPlainTermList();
        int max = plainTermList.size() > 10 ? 10 : plainTermList.size();
        List<String> resultList = plainTermList.subList(0, max);
        for(int i = 0; i<resultList.size(); i++) {
            resultList.set(i, this.parentClusterQuery + " " + resultList.get(i));
        }
        return resultList;
    }

    private List<String> createPlainTermList() {
        List<String> importantTerms = this.termOccurences.get(0);
        for(List<String> termList : this.termOccurences.subList(1, this.termOccurences.size() - 1)) {
            importantTerms.removeAll(termList);
        }

        return importantTerms;
    }
    public void cleanUp() {
    }
}
