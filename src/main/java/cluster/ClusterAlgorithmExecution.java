package cluster;

import common.AnalyzerFactory;
import common.CommonConstants;
import common.IndexDirectoryBuilder;
import common.StemmerFactory;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

/**
 * Created by marc on 09.06.17.
 * This class manages the execution of the clustering algorithm.
 */
public class ClusterAlgorithmExecution {

    private final IndexDirectoryBuilder indexDirectoryBuilder;
    private final AnalyzerFactory analyzerFactory;
    private final StemmerFactory stemmerFactory;
    private IndexSearcher indexSearcher;
    private final Scanner stdInScanner;
    private ClusterAlgorithm clusterAlgorithm;

    public ClusterAlgorithmExecution(IndexDirectoryBuilder indexDirectoryBuilder,
                                     AnalyzerFactory analyzerFactory,
                                     StemmerFactory stemmerFactory) {
        this.analyzerFactory = analyzerFactory;
        this.stemmerFactory = stemmerFactory;
        this.stdInScanner = new Scanner(System.in);
        this.indexDirectoryBuilder = indexDirectoryBuilder;
    }

    public ClusterAlgorithmExecution initialize() {
        try {
            this.indexSearcher = this.createSearcher();
            this.clusterAlgorithm = new ClusterAlgorithm(this.indexSearcher, this.analyzerFactory.createAnalyzer(), this.stemmerFactory);
        } catch (IOException ex) {
            System.out.println("Could not create index searcher " + ex.getMessage());
        }
        return this;
    }

    public ClusterAlgorithmExecution start() {
        while (true) {
            String query = this.requestUserQuery();
            if (query.equals(CommonConstants.EXIT_WORD)) {
                return this;
            }
            this.runClusteringProcess(query);
            this.cleanUp();
        }
    }

    private void cleanUp() {
    }

    private String requestUserQuery() {
        System.out.println("Enter a query (exit to stop):");
        String query = this.stdInScanner.next();
        if (query.equals(CommonConstants.EXIT_WORD)) {
            return CommonConstants.EXIT_WORD;
        }
        return query;
    }

    private ClusteringResult clusterQuery(String query) {
        try {
            System.out.println("Running cluster algorithm with query: " + query);
            return this.clusterAlgorithm.cluster(query);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String requestUserToSelectCluster(ClusteringResult result) {
        System.out.println("Results (descending) for " + result.getClusterQuery());
        System.out.println("Found files:");
        for (String fileName : result.getMatchedFiles()) {
            System.out.println(fileName);
        }
        System.out.println("Suggested cluster:");
        List<String> suggestedCluster = result.getSuggestedCluster();
        for (int i = 0; i < suggestedCluster.size(); i++) {
            System.out.println(i + " " + suggestedCluster.get(i));
        }
        System.out.println("Cluster number (-1 to stop):");
        int clusterNumber = this.stdInScanner.nextInt();
        if (clusterNumber == -1) {
            return CommonConstants.EXIT_WORD;
        }
        return suggestedCluster.get(clusterNumber);
    }

    private void runClusteringProcess(String query) {
        ClusteringResult result = this.clusterQuery(query);
        String nextQuery = this.requestUserToSelectCluster(result);
        if (nextQuery.equals(CommonConstants.EXIT_WORD)) {
            return;
        }
        this.runClusteringProcess(nextQuery);
    }

    private IndexSearcher createSearcher() throws IOException {
        Directory dir = this.indexDirectoryBuilder.getDirectory();
        IndexReader reader = DirectoryReader.open(dir);
        return new IndexSearcher(reader);
    }
}
