package cluster;

import common.LuceneConstants;
import common.StemmerFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.simple.SimpleQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import java.io.IOException;
import java.util.List;

/**
 * Created by marc on 09.06.17.
 * Implements the clustering algorithm.
 */
class ClusterAlgorithm {

    private final IndexSearcher indexSearcher;
    private final Analyzer analyzer;
    private final StemmerFactory stemmerFactory;

    ClusterAlgorithm(IndexSearcher indexSearcher,
                     Analyzer analyzer,
                     StemmerFactory stemmerFactory) {
        this.indexSearcher = indexSearcher;
        this.analyzer = analyzer;
        this.stemmerFactory = stemmerFactory;
    }

    ClusteringResult cluster(String query) throws IOException {
        ClusteringResult clusteringResult = new ClusteringResult(query);
        SubClusterBuilder subClusterBuilder = new StandardKeywordGenerator(query, this.analyzer, this.stemmerFactory);// new MoreLikeThisSubClusterBuilder(query, this.indexSearcher.getIndexReader(), this.analyzer);//
        SimpleQueryParser simpleQueryParser = new SimpleQueryParser(this.analyzer, LuceneConstants.CONTENTS);
        Query parse = simpleQueryParser.parse(query);

        TopDocs search = this.indexSearcher.search(parse, 1000);
        for (ScoreDoc scoreDoc: search.scoreDocs) {
                Document doc = this.indexSearcher.doc(scoreDoc.doc);
                clusteringResult.addMatchedFile(doc.getField(LuceneConstants.FILE_NAME).stringValue());
                subClusterBuilder.analyzeDocument(doc, scoreDoc.doc);
        }

        List<String> top10ImportantTerms =
                subClusterBuilder.getTop10ImportantTerms();
        clusteringResult.addSuggestedClusters(top10ImportantTerms);
        return clusteringResult;
    }
}
